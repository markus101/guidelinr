guideLinr v1.0.1

guideLinr is a Chrome extension that adds simple, lightweight, configurable Photoshop-like guide lines to any page.

With guideLinr, you can create vertical or horizontal guidelines that will persist by URL.  If you visit Google today and add guides, tomorrow they'll be back when you return.

There are several different color options available, and the color of guides can be changed from the rich context menu available on each guide.

There is an integrated distance measurement tool that shows distance between your guides as you move them.  This is especially helpful when aligning guides to elements on your page.

guideLinr guides can be dragged with your pointer, or moved with your arrow keys for more precise alignment.  Hold Shift while you press arrow keys, and they move faster.

There's also a "Snap To Grid" feature that helps align lines to a grid of your choosing.

The source for guideLinr is available here:  https://bitbucket.org/slobaum/guidelinr/overview