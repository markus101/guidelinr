##############################################------##
############################################## OPEN  #
##############################################------##

Hide guides (instead of/in addition to remove)
    This should be by color (hide green, show blue)
    And you should be able to "Hide All/Show All"

Click/Drag measurements to move two guides at once

Option to include location hash when saving

"Save Layout"/"Load Layout" to move layouts across URLs.
    You should be able to decide which colors you want as part of your saved layout.
        "Save this layout... just the blue ones."
        "Save this layout;  blue, orange, and red, no others."
    Once layouts are saved, can you share them?
        This could be big, collaborative guidelines.
        Teams sharing guideline layouts.
        Maybe this is too far :)

Convert from PX to %?

Accept donations?
    Can we do this via in-app purchases?
    http://blog.chromium.org/2014/03/new-monetization-and-publishing-options_11.html

##############################################------##
############################################## DONE  #
##############################################------##

Fix nudging with snapping off :(
    DONE: 2014-01-07 :)

Context menu entry?
    This will be primarily to add guides (at first).
    Can this be optional?  Checkbox in the interface to turn on/off?
    Can we take this farther?  Right click element, then click "Surround" to add four guides at the element boundaries?
        This could be wishful thinking ;)
    DONE: 2014-02-08 :)
        Now there are context menu entries for Vertical, Horizontal, and Surround!!
        Unfortunately, this is not optional at this point.  I would like to add that.  Will update here if it happens.
    Done at some point recently :P
        Context menus are optional based on checkbox in interface.

Should distance measurement elements be fixed instead of absolute?
    DONE: 2014-02-09 :)
        They TOTALLY should!  Much more usable, I think.

Snap to elements (probably won't happen...)
    DONE: 2014-02-09 :)
        Holy.  Crap.  I really didn't see this one happening, as you can see above.
        Also optional.
